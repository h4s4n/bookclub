import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'

function SearchBox() {
    const [keyword, setKeyword] = useState('')

    let history = useHistory()

    const submitHandler = (e) => {
        e.preventDefault()
        if (keyword) {
            history.push(`/?keyword=${keyword}&page=1`)
        } else {
            history.push(history.push(history.location.pathname))
        }
    }
    const record = () => {
        const script = document.createElement("script");
        const SpeechRecognition =
          window.SpeechRecognition || window.webkitSpeechRecognition;
        const recognition = new SpeechRecognition();
        recognition.lang = "en-GB";
        console.log(" recording")
        recognition.onresult = function (event) {
            console.log(event)
            document.getElementById("qe").value =
              event.results[0][0].transcript;
            }
        recognition.start();
        }
        
   
    return (
      <Form onSubmit={submitHandler} inline>
        <Form.Control
          id="qe"
          type="text"
          name="q"
          onBlur={(e) => setKeyword(e.target.value)}
          className="mr-sm-2 ml-sm-5"
        ></Form.Control>

        {/* <Button type="submit" variant="outline-success" className="p-2">
          Search
        </Button> */}
        <Button type="submit" variant="dark" className="btn-sm">
          <i class="fas fa-search"></i>
        </Button>
        <Button variant="dark" className="btn-sm" onClick={record}>
          <i className="fas fa-microphone"></i>
        </Button>
      </Form>
    );
}

export default SearchBox
