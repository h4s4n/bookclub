import React, { useState, useEffect } from 'react'
import { Form, Button, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { savePaymentMethod } from '../actions/cartActions'

function PaymentScreen({ location, history }) {

    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    const dispatch = useDispatch()

          
    const [paymentMethod, setPaymentMethod] = useState()
    let test = location.search ? Number(location.search.split("?")[1]) : 0;
    var bool = false;
    const a = 1;
    // var type='hardcover'
    console.log(test);
    if (test === 1) {
      // console.log('hel  llo ')
      // qty = 1;
      // console.log(qty);
      bool = true;
      // type='pdf'
      console.log(bool);
    } 
          

    if (!shippingAddress.address & !bool) {
        history.push('/shipping')
    }

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(savePaymentMethod(paymentMethod))
        {bool ? history.push("/placeorder?1") : history.push("/placeorder");}
        
    }

    return (
      <FormContainer>
        <CheckoutSteps step1 step2 step3 />

        <Form onSubmit={submitHandler}>
          <Form.Group>
            <Form.Label as="legend">Select Method</Form.Label>
            <Col>
              <Form.Check
                type="radio"
                label="PayPal or Credit Card"
                id="paypal"
                value="paypal"
                name="paymentMethod"
                
                onChange={(e) => setPaymentMethod(e.target.value)}
              ></Form.Check>
            </Col>

            <Col>
              <Form.Check
                type="radio"
                label="Cash on delivery"
                id="COD"
                value="COD"
                name="paymentMethod"
                onChange={(e) => setPaymentMethod(e.target.value)}
              ></Form.Check>
            </Col>
          </Form.Group>
          
          <Button type="submit" variant="primary">
            Continue
          </Button>
        </Form>
      </FormContainer>
    );
}

export default PaymentScreen
