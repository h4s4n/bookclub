import React, { useState, useEffect } from 'react'
import { Button, Row, Col, ListGroup, Image, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { PayPalButton } from 'react-paypal-button-v2'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { getOrderDetails, payOrder, deliverOrder } from '../actions/orderActions'
import { ORDER_PAY_RESET, ORDER_DELIVER_RESET } from '../constants/orderConstants'
import { Redirect } from "react-router"
import axios from "axios"
function OrderScreen({ match, location, history }) {
const orderId = match.params.id;
const dispatch = useDispatch();

const [sdkReady, setSdkReady] = useState(false);
const orderDetails = useSelector((state) => state.orderDetails);
const { order, error, loading } = orderDetails;
let test = location.search ? Number(location.search.split("=")[1]) : 1;
var bool = false;

const orderPay = useSelector((state) => state.orderPay);
const { loading: loadingPay, success: successPay } = orderPay;
const orderDeliver = useSelector((state) => state.orderDeliver);
const { loading: loadingDeliver, success: successDeliver } = orderDeliver;
const userLogin = useSelector((state) => state.userLogin);
const { userInfo } = userLogin;



const pdfDownloadHandler = (value) => () => {
  console.log(value);
   
   axios({
      // url: "http://localhost:3000/static/images/5-240.Academic_Calendar_Fall_2021_CtHkEEO.pdf",
     url: "http://localhost:3000/static"+value,
     method: "GET",
     responseType: "blob", // important
   }).then((response) => {
     const url = window.URL.createObjectURL(new Blob([response.data]));
     const link = document.createElement("a");
     link.href = url;
     link.setAttribute("download", "file.pdf");
     document.body.appendChild(link);
     link.click();
   });
};

if (!loading && !error) {
  console.log("print item:", order.itemsPrice)
}
  
if (!loading && !error) {
  // {
  //   bool
  //     ? (order.itemsPrice = order.orderItems
  //         .reduce((acc, item) => acc + item.pdfPrice * item.qty, 0)
  //         .toFixed(2))
  //     : (order.itemsPrice = order.orderItems
  //         .reduce((acc, item) => acc + item.price * item.qty, 0)
  //         .toFixed(2));
  // }
  order.itemsPrice = order.orderItems
    .reduce((acc, item) => acc + item.price * item.qty, 0)
    .toFixed(2);
}
console.log("secret:", orderId);
// console.log("this is pdf price ",order.itemsPrice)

const addPayPalScript = () => {
  const script = document.createElement("script");
  script.type = "text/javascript";
  script.src =
    "https://www.paypal.com/sdk/js?client-id=AcBFdGWbxo_507OHYMNYjh6H6FGnTwmf-Q2mF4YGVtEHGaiHXCfGAUjANG2WMnZm6Mcp4oXBkzKmBe2J";
  script.async = true;
  script.onload = () => {
    setSdkReady(true);
  };
  document.body.appendChild(script);
};
useEffect(() => {
  if (!userInfo) {
    history.push("/login");
  }
  if (
    !order ||
    successPay ||
    order._id !== Number(orderId) ||
    successDeliver
  ) {
    dispatch({ type: ORDER_PAY_RESET });
    dispatch({ type: ORDER_DELIVER_RESET });
    dispatch(getOrderDetails(orderId));
  } else if (!order.isPaid) {
    if (!window.paypal) {
      addPayPalScript();
    } else {
      setSdkReady(true);
    }
  }
}, [dispatch, order, orderId, successPay, successDeliver]);
const successPaymentHandler = (paymentResult) => {
  dispatch(payOrder(orderId, paymentResult));
};
const deliverHandler = () => {
  dispatch(deliverOrder(order));
};

console.log("orderpay", orderPay) 
if (!loading && !error) {
  if (order.method === "pdf") {
    // console.log('hel  llo ')
    var bool = true;
    console.log("order", bool);
  }
}

// console.log("paid or not", order.id )

// console.log("something", orederDetails)



return loading ? (
  <Loader />
) : error ? (
  <Message variant="danger">{error}</Message>
) : (
  <div>
    <h1>Order: {order.Id}</h1>
    <Row>
      <Col md={8}>
        <ListGroup variant="flush">
          <ListGroup.Item>
            <h2>Shipping</h2>
            <p>
              <strong>Name: </strong> {order.user.name}
            </p>
            <p>
              <strong>Email: </strong>
              <a href={`mailto:${order.user.email}`}>{order.user.email}</a>
            </p>
            {bool ? (
              <p></p>
            ) : (
              <p>
                <strong>Shipping: </strong>
                {order.shippingAddress.address}, {order.shippingAddress.city}
                {"  "}
                {order.shippingAddress.postalCode},{"  "}
                {order.shippingAddress.district}
              </p>
            )}
            {order.isDelivered & !bool ? (
              <Message variant="success">
                Delivered on {order.deliveredAt}
              </Message>
            ) : order.isDelivered & order.isPaid & bool ? (
              <Message variant="success">Delivered</Message>
            ) : (
              <Message variant="warning">Not Delivered</Message>
            )}
          </ListGroup.Item>
          <ListGroup.Item>
            <h2>Payment Method</h2>
            <p>
              <strong>Method: </strong>
              {order.paymentMethod}
            </p>
            {order.isPaid ? (
              <Message variant="success">Paid on {order.paidAt}</Message>
            ) : (
              <Message variant="warning">Not Paid</Message>
            )}
          </ListGroup.Item>
          {/* {bool ? (
            <Message variant="success">pdfdfff {order.paidAt}</Message>
          ) : (
            <Message variant="warning">Not pdfdfdfdfdf</Message>
          )} */}
          <ListGroup.Item>
            <h2>Order Items</h2>
            {order.orderItems.length === 0 ? (
              <Message variant="info">Order is empty</Message>
            ) : (
              <ListGroup variant="flush">
                {order.orderItems.map((item, index) => (
                  <ListGroup.Item key={index}>
                    <Row>
                      <Col md={1}>
                        <Image
                          // src={item.image}
                          src={item.image}
                          alt={item.name}
                          fluid
                          rounded
                        />
                      </Col>
                      
                      <Col>
                        <Link to={`/product/${item.product}`}>
                          {/* <Link to={"http://localhost:3000/static"+item.pdf} */}
                          {item.name}
                        </Link>
                      </Col>
                      {bool && order.isPaid && userInfo && !userInfo.isAdmin ? (
                        <Col>
                          <Button
                            variant="dark"
                            className="btn-sm"
                            onClick={pdfDownloadHandler(item.pdf)}
                          >
                            Download
                          </Button>
                        </Col>
                      ) : (
                        <Col></Col>
                      )}
                      {bool ? (
                        <Col md={4}>
                          {item.qty} X ৳{item.price} = ৳
                          {(item.qty * item.price).toFixed(2)}
                        </Col>
                      ) : (
                        <Col md={4}>
                          {item.qty} X ৳{item.price} = ৳
                          {(item.qty * item.price).toFixed(2)}
                        </Col>
                      )}
                    </Row>
                  </ListGroup.Item>
                ))}
              </ListGroup>
            )}
          </ListGroup.Item>
        </ListGroup>
      </Col>
      <Col md={4}>
        <Card>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h2>Order Summary</h2>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Items:</Col>
                <Col>৳{order.itemsPrice}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Shipping:</Col>
                <Col>৳{order.shippingPrice}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Tax:</Col>
                <Col>৳{order.taxPrice}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Total:</Col>
                <Col>৳{order.totalPrice}</Col>
              </Row>
            </ListGroup.Item>
            {!order.isPaid && (
              <ListGroup.Item>
                {loadingPay && <Loader />}
                {!sdkReady ? (
                  <Loader />
                ) : (
                  <PayPalButton
                    amount={order.totalPrice}
                    onSuccess={successPaymentHandler}
                  />
                )}
              </ListGroup.Item>
            )}
          </ListGroup>
          {loadingDeliver && <Loader />}
          {userInfo &&
            userInfo.isAdmin &&
            order.isPaid &&
            !order.isDelivered && (
              <ListGroup.Item>
                <Button
                  type="button"
                  className="btn btn-block"
                  onClick={deliverHandler}
                >
                  Mark As Delivered
                </Button>
              </ListGroup.Item>
            )}
        </Card>
      </Col>
    </Row>
  </div>
);
}

export default OrderScreen
